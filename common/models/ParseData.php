<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * This is the model class for table "parse_data".
 *
 * @property integer $id
 * @property string $file
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 */
class ParseData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parse_data';
    }

    const FILE_UPLOAD_DIR = "uploads/pars/";
    public $parsFile;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['file'], 'string', 'max' => 15],
            [
                ['parsFile'],
                'file',
                'skipOnEmpty' => !$this->isNewRecord,
                'maxSize' => 1024*1024,
                'message' => 'Вы не указали файл.'
            ]
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'Файл',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'comment' => 'Коментарий',
        ];
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->parsFile = UploadedFile::getInstance($this, 'parsFile');

        if (!empty($this->parsFile->extension)) {
            $this->file = $this->parsFile->extension;
        }
        if ($this->isNewRecord) {
            $this->created_at = time();
        }
        $this->updated_at = time();
        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if(!empty($this->parsFile)){
            $this->parsFile->saveAs(self::FILE_UPLOAD_DIR . $this->id . '.' . $this->parsFile->extension);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     *
     */
    public function afterDelete()
    {
        if(file_exists(self::FILE_UPLOAD_DIR . $this->id . '.' . $this->file)){
            unlink(self::FILE_UPLOAD_DIR . $this->id . '.' . $this->file);
        }
        parent::afterDelete();
    }

    public function getArrayToGraphic(){

        //Парсим данные из файла
        $model = new \frontend\models\ParserForm();
        $model->url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/' .self::FILE_UPLOAD_DIR . $this->id . '.' . $this->file;
        $data = $model->data;


        //Получаем начальное значение
        $balance = 0;
        foreach($data as $key => $vals){
            if(strcasecmp($data[$key][2], "balance") == 0 && (float)$data[$key][13] > 0){
                $balance = (float)$data[$key][13];
                break;
            }
        }

        //Строим массив для точек графика, по оси x идет профит, по оси y тикет
        $arrayXY = [];
        $i = 0;
        foreach($data as $key => $vals){
            if(strcasecmp(trim($data[$key][2]), "buy") == 0 && !empty($data[$key][13])){
                $arrayXY[$i]['x'] = $balance + ((float)$data[$key][13]);
                $arrayXY[$i]['y'] = $data[$key][0];
            }
            ++$i;
        }

        return $arrayXY;

    }

}
