<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use GuzzleHttp\Client;
use yii\helpers\Url;


class ParserForm extends Model
{
    public $client;
    public $url;
    public $maxLength = 0;
    public $tableData = [];

    public function init()
    {
        $this->client = new Client();
        parent::init();
    }

    public function getData(){
        //Тянем данные
        $res = $this->client->request('GET', $this->url);
        $document = \phpQuery::newDocumentHTML($res->getBody());
        $body = $document->find("tr");

        $i = 0;
        foreach($body as $element){
            $idElem = $element->getElementsByTagName('td');
            for($num=0; $num<$idElem->length; ++$num){
                if($idElem->item($num)->getAttribute('colspan')>1){
                    $count = $idElem->item($num)->getAttribute('colspan');
                    --$count;
                    for($numColspan=0; $numColspan<$count; ++$numColspan){
                        $this->tableData[$i][] = null;
                    }
                }
                $this->tableData[$i][] = $idElem->item($num)->nodeValue;
            }
            $this->maxLength = count($this->tableData[$i])>$this->maxLength ? count($this->tableData[$i]) : $this->maxLength;
            ++$i;
        }


        //Дополняем массив пустыми данными что бы проще было достроить
        foreach($this->tableData as $key=>$arr){
            for($i=0; $i<($this->maxLength);++$i){
                if(isset($arr[$i])){ continue; }
                $this->tableData[$key][$i] = '';
            }
        }

        return $this->tableData;
    }


}
