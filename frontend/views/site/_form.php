<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use common\models\ParseData;

/* @var $this yii\web\View */
/* @var $model common\models\ParseData */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parse-data-form">

    <?php $form = ActiveForm::begin(['id' => 'import-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>


    <?php
    echo $form->field($model, 'comment')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]);
    ?>


    <?php
    if (!empty($model->file) && file_exists(ParseData::FILE_UPLOAD_DIR . $model->id . '.' . $model->file)) {
        echo 'Загружено: ' . Html::a($model->id . '.' . $model->file,
            '/' . ParseData::FILE_UPLOAD_DIR . $model->id . '.' . $model->file, ['target' => '_blank']);
    }
    ?>
    <?= $form->field($model, 'parsFile')->fileInput()->label('Загрузить файл') ?>

    <div class="form-group formBtnSend">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

