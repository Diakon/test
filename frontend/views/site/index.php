<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ParseData;

/* @var $this yii\web\View */

$this->title = 'Тестовое задание Склярова П. Н.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">


    <div class="parse-data-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Загрузить новые данные', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',

                [
                    'attribute' => 'created_at',
                    'content' => function ($data) {
                        if (file_exists(ParseData::FILE_UPLOAD_DIR . $data->id . '.' . $data->file)) {
                            return Html::a($data->id . '.' . $data->file,
                                '/' . ParseData::FILE_UPLOAD_DIR . $data->id . '.' . $data->file, ['target' => '_blank']);
                        }
                    }
                ],

                [
                    'attribute' => 'comment',
                    'format' => 'html',
                ],


                [
                    'attribute' => 'created_at',
                    'content' => function ($data) {
                        return date('d-m-Y H:i:s', $data->created_at);
                    }
                ],

                [
                    'attribute' => 'updated_at',
                    'content' => function ($data) {
                        return date('d-m-Y H:i:s', $data->updated_at);
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'headerOptions' => ['width' => '80'],
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-stats"></span>',
                                $url);
                        },
                        'link' => function ($url, $model, $key) {
                            return Html::a('Действие', $url);
                        },
                    ],
                ],
            ],
        ]); ?>

    </div>


</div>
