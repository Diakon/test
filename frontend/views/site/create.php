<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParseData */

$this->title = 'Спарсить новые данные';
$this->params['breadcrumbs'][] = ['label' => 'Parse Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parse-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
