<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ParseData */

$this->title = 'График по данным файла '.$model->id.'.'.$model->file;
$this->params['breadcrumbs'][] = ['label' => 'График', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->getArrayToGraphic()

?>
<div class="parse-data-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <script>
        //Создаем данные точек по щаблону
        var chartData = [
            <?php
            foreach($model->getArrayToGraphic() as $values){?>
            {
                "ticket": "<?=$values['y'];?>",
                "profit": "<?=$values['x'];?>"
            },
            <?php } ?>
        ];

        AmCharts.ready(function () {
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "ticket";
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 90;
            categoryAxis.gridPosition = "start";

            // value
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "profit";
            graph.balloonText = "Тикет: [[category]]<BR>Баланс: [[value]]";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);

            chart.creditsPosition = "top-right";

            chart.write("chartdiv");
        });
    </script>

    <div id="chartdiv" style="width:100%; height:500px;"></div>

</div>
