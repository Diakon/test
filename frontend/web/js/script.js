$('#parsedata-url').on('change', function(){

    var alpha = {};
    alpha[0] = 'A';
    alpha[1] = 'B';
    alpha[2] = 'C';
    alpha[3] = 'D';
    alpha[4] = 'E';
    alpha[5] = 'F';
    alpha[6] = 'G';
    alpha[7] = 'H';
    alpha[8] = 'I';
    alpha[9] = 'J';
    alpha[10] = 'K';
    alpha[11] = 'L';
    alpha[12] = 'M';
    alpha[13] = 'N';
    alpha[14] = 'O';
    alpha[15] = 'P';
    alpha[16] = 'Q';
    alpha[17] = 'R';
    alpha[18] = 'S';
    alpha[19] = 'T';
    alpha[20] = 'X';
    alpha[21] = 'W';
    alpha[22] = 'Z';

    $.ajax({
        type: 'POST',
        url: '/site/ajaxurl',
        dataType: 'json',
        data: $('#w0').serialize(),
        success: function (data) {
            //Пришли данные - строим таблицу
            var html = '<h3>Будут спарсены следующие данные:</h3>';
            html += '<table class="table table-striped table-bordered">';

            //Строим ячейки
            html += '<thead>';
            html += '<tr>';
            html += '<td class="row_num"></td>';
            $.each( data[0], function( key, value ) {
                html += '<th class="row_num">';
                html += alpha[key];
                html += '</th>';
            });
            html += '</tr>';
            html += '</thead>';

            //Выводим ячейки
            html += '<tbody>';
            $.each( data, function( keys, values ) {
                html += '<tr>';
                html += '<td class="row_num"><b>'+keys+'</b></td>';
                $.each( values, function( key, value ) {
                    html += '<td>'+value+'</td>';
                });
                html += '</tr>';
            });
            html += '</tbody>';


            html += '</table>';


            $('#table').show();
            $('#table').empty().append(html);
            $('.formBtnSend').show();
        }
    });

});

$(document).on('click', '.glyphicon-info-sign', function(){
    alert($(this).data('help'));
});