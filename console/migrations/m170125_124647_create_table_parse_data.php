<?php

use yii\db\Schema;
use yii\db\Migration;

class m170125_124647_create_table_parse_data extends Migration
{
    public function up()
    {
        $this->createTable('{{parse_data}}', [
            'id' => $this->primaryKey(),
            'file' => $this->string(15)->notNull(),
            'comment' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('IND_parse_data_url', '{{parse_data}}', 'url');

    }

    public function down()
    {
        $this->dropTable('{{parse_data}}');

        return false;
    }


}
